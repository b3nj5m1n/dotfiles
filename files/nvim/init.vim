
" Plugin manager
source ~/.config/nvim/plug-conf/plugins.vim

" General Settings
source ~/.config/nvim/general/settings.vim

" Key mappings
source ~/.config/nvim/general/mappings.vim



" Plugin Configs

source ~/.config/nvim/plug-conf/coc.vim

" source ~/.config/nvim/plug-conf/nerd-tree.vim

source ~/.config/nvim/plug-conf/quick-scope.vim

source ~/.config/nvim/plug-conf/sneak.vim

source ~/.config/nvim/plug-conf/airline.vim

source ~/.config/nvim/plug-conf/fzf.vim
