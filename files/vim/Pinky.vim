hi clear
syntax reset
let g:colors_name = "my-scheme"
set background=dark
set t_Co=256
hi Normal guifg=#f5f5f5 ctermbg=NONE guibg=#171517 gui=NONE

hi DiffText guifg=#ff0066 guibg=NONE
hi ErrorMsg guifg=#ff0066 guibg=NONE
hi WarningMsg guifg=#ff0066 guibg=NONE
hi PreProc guifg=#ff0066 guibg=NONE
hi Exception guifg=#ff0066 guibg=NONE
hi Error guifg=#ff0066 guibg=NONE
hi DiffDelete guifg=#ff0066 guibg=NONE
hi GitGutterDelete guifg=#ff0066 guibg=NONE
hi GitGutterChangeDelete guifg=#ff0066 guibg=NONE
hi cssIdentifier guifg=#ff0066 guibg=NONE
hi cssImportant guifg=#ff0066 guibg=NONE
hi Type guifg=#ff0066 guibg=NONE
hi Identifier guifg=#ff0066 guibg=NONE
hi PMenuSel guifg=#00ff99 guibg=NONE
hi Constant guifg=#00ff99 guibg=NONE
hi Repeat guifg=#00ff99 guibg=NONE
hi DiffAdd guifg=#00ff99 guibg=NONE
hi GitGutterAdd guifg=#00ff99 guibg=NONE
hi cssIncludeKeyword guifg=#00ff99 guibg=NONE
hi Keyword guifg=#00ff99 guibg=NONE
hi IncSearch guifg=#f0e100 guibg=NONE
hi Title guifg=#f0e100 guibg=NONE
hi PreCondit guifg=#f0e100 guibg=NONE
hi Debug guifg=#f0e100 guibg=NONE
hi SpecialChar guifg=#f0e100 guibg=NONE
hi Conditional guifg=#f0e100 guibg=NONE
hi Todo guifg=#f0e100 guibg=NONE
hi Special guifg=#f0e100 guibg=NONE
hi Label guifg=#f0e100 guibg=NONE
hi Delimiter guifg=#f0e100 guibg=NONE
hi Number guifg=#f0e100 guibg=NONE
hi CursorLineNR guifg=#f0e100 guibg=NONE
hi Define guifg=#f0e100 guibg=NONE
hi MoreMsg guifg=#f0e100 guibg=NONE
hi Tag guifg=#f0e100 guibg=NONE
hi String guifg=#f0e100 guibg=NONE
hi MatchParen guifg=#f0e100 guibg=NONE
hi Macro guifg=#f0e100 guibg=NONE
hi DiffChange guifg=#f0e100 guibg=NONE
hi GitGutterChange guifg=#f0e100 guibg=NONE
hi cssColor guifg=#f0e100 guibg=NONE
hi Function guifg=#00b4d8 guibg=NONE
hi Directory guifg=#f85e00 guibg=NONE
hi markdownLinkText guifg=#f85e00 guibg=NONE
hi javaScriptBoolean guifg=#f85e00 guibg=NONE
hi Include guifg=#f85e00 guibg=NONE
hi Storage guifg=#f85e00 guibg=NONE
hi cssClassName guifg=#f85e00 guibg=NONE
hi cssClassNameDot guifg=#f85e00 guibg=NONE
hi Statement guifg=#ff1b1c guibg=NONE
hi Operator guifg=#ff1b1c guibg=NONE
hi cssAttr guifg=#ff1b1c guibg=NONE


hi Pmenu guifg=#f5f5f5 guibg=#700353
hi SignColumn guibg=#171517
hi Title guifg=#f5f5f5
hi LineNr guifg=#8f8f8f guibg=#171517
hi NonText guifg=#1affa3 guibg=#171517
hi Comment guifg=#1affa3 gui=italic
hi SpecialComment guifg=#1affa3 gui=italic guibg=#171517
hi CursorLine guibg=#700353
hi TabLineFill gui=NONE guibg=#700353
hi TabLine guifg=#8f8f8f guibg=#700353 gui=NONE
hi StatusLine gui=bold guibg=#700353 guifg=#f5f5f5
hi StatusLineNC gui=NONE guibg=#171517 guifg=#f5f5f5
hi Search guibg=#1affa3 guifg=#f5f5f5
hi VertSplit gui=NONE guifg=#700353 guibg=NONE
hi Visual gui=NONE guibg=#700353
