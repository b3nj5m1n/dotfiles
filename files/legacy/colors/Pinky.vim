hi clear
syntax reset
let g:colors_name = "Pinky"
set background=dark
set t_Co=256
hi Normal guifg=#f5f5f5 ctermbg=NONE guibg=#171517 gui=NONE

hi DiffText guifg=#ff0066 guibg=NONE
hi ErrorMsg guifg=#ff0066 guibg=NONE
hi WarningMsg guifg=#ff0066 guibg=NONE
hi PreProc guifg=#ff0066 guibg=NONE
hi Exception guifg=#ff0066 guibg=NONE
hi Error guifg=#ff0066 guibg=NONE
hi DiffDelete guifg=#ff0066 guibg=NONE
hi GitGutterDelete guifg=#ff0066 guibg=NONE
hi GitGutterChangeDelete guifg=#ff0066 guibg=NONE
hi cssIdentifier guifg=#ff0066 guibg=NONE
hi cssImportant guifg=#ff0066 guibg=NONE
hi Type guifg=#ff0066 guibg=NONE
hi Identifier guifg=#ff0066 guibg=NONE
hi PMenuSel guifg=#00ff99 guibg=NONE
hi Constant guifg=#00ff99 guibg=NONE
hi Repeat guifg=#00ff99 guibg=NONE
hi DiffAdd guifg=#00ff99 guibg=NONE
hi GitGutterAdd guifg=#00ff99 guibg=NONE
hi cssIncludeKeyword guifg=#00ff99 guibg=NONE
hi Keyword guifg=#00ff99 guibg=NONE
hi IncSearch guifg=#eb005e guibg=NONE
hi Title guifg=#eb005e guibg=NONE
hi PreCondit guifg=#eb005e guibg=NONE
hi Debug guifg=#eb005e guibg=NONE
hi SpecialChar guifg=#eb005e guibg=NONE
hi Conditional guifg=#eb005e guibg=NONE
hi Todo guifg=#eb005e guibg=NONE
hi Special guifg=#eb005e guibg=NONE
hi Label guifg=#eb005e guibg=NONE
hi Delimiter guifg=#eb005e guibg=NONE
hi Number guifg=#eb005e guibg=NONE
hi CursorLineNR guifg=#eb005e guibg=NONE
hi Define guifg=#eb005e guibg=NONE
hi MoreMsg guifg=#eb005e guibg=NONE
hi Tag guifg=#eb005e guibg=NONE
hi String guifg=#eb005e guibg=NONE
hi MatchParen guifg=#eb005e guibg=NONE
hi Macro guifg=#eb005e guibg=NONE
hi DiffChange guifg=#eb005e guibg=NONE
hi GitGutterChange guifg=#eb005e guibg=NONE
hi cssColor guifg=#eb005e guibg=NONE
hi Function guifg=#00eb8d guibg=NONE
hi Directory guifg=#c4004e guibg=NONE
hi markdownLinkText guifg=#c4004e guibg=NONE
hi javaScriptBoolean guifg=#c4004e guibg=NONE
hi Include guifg=#c4004e guibg=NONE
hi Storage guifg=#c4004e guibg=NONE
hi cssClassName guifg=#c4004e guibg=NONE
hi cssClassNameDot guifg=#c4004e guibg=NONE
hi Statement guifg=#00c476 guibg=NONE
hi Operator guifg=#00c476 guibg=NONE
hi cssAttr guifg=#00c476 guibg=NONE


hi Pmenu guifg=#f5f5f5 guibg=#9900ff
hi SignColumn guibg=#171517
hi Title guifg=#f5f5f5
hi LineNr guifg=#8f8f8f guibg=#171517
hi NonText guifg=#00e6ff guibg=#171517
hi Comment guifg=#00e6ff gui=italic
hi SpecialComment guifg=#00e6ff gui=italic guibg=#171517
hi CursorLine guibg=#9900ff
hi TabLineFill gui=NONE guibg=#9900ff
hi TabLine guifg=#8f8f8f guibg=#9900ff gui=NONE
hi StatusLine gui=bold guibg=#9900ff guifg=#f5f5f5
hi StatusLineNC gui=NONE guibg=#171517 guifg=#f5f5f5
hi Search guibg=#00e6ff guifg=#f5f5f5
hi VertSplit gui=NONE guifg=#9900ff guibg=NONE
hi Visual gui=NONE guibg=#9900ff
