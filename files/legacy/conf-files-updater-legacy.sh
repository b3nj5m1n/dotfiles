#!/bin/bash

# All the config stuff I don't actively use anymore

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
USER=/home/$(echo $DIR | cut -d/ -f3)

declare -A loc0=(
    [name]="bashrc"
    [local]="./bash/.bashrc"
    [remote]="$USER"
)
declare -A loc4=(
    [name]="GTK themes"
    [local]="./.themes/*"
    [remote]="$USER/.themes/"
)
declare -A loc6=(
    [name]="fish"
    [local]="./fish/config.fish"
    [remote]="$USER/.config/fish/"
)
declare -A loc8=(
    [name]="konsole"
    [local]="./konsole/*"
    [remote]="$USER/.local/share/konsole/"
)
declare -A loc9=(
    [name]="vimrc"
    [local]="./vim/.vimrc"
    [remote]="$USER/"
)
declare -A loc19=(
    [name]="wallpapers"
    [local]="./wallpapers/.wallpapers/*"
    [remote]="/usr/share/wallpapers/custom/"
)
declare -A loc21=(
    [name]="i3"
    [local]="./i3/config"
    [remote]="$USER/.config/i3/"
)
declare -A loc23=(
    [name]="rofi config"
    [local]="./rofi/config.rasi"
    [remote]="$USER/.config/rofi/"
)
declare -A loc24=(
    [name]="Desktop entries"
    [local]="./xsessions/*"
    [remote]="/usr/share/xsessions/"
)
declare -A loc26=(
    [name]="dwm autostart"
    [local]="./dwm/*"
    [remote]="$USER/.dwm/"
)
declare -A loc27=(
    [name]="dwmblocks scripts"
    [local]="./scripts/dwmblocks/*"
    [remote]="$USER/dwmblocks/"
)

RED='\033[0;31m'
GREEN='\033[0;32m'
PURPLE='\033[1;35m'
REVERSE='\033[7m'
RESET='\033[0m'

declare -n loc
for loc in ${!loc@}; do
    printf "${PURPLE} Transfering ${REVERSE}${loc[name]}${RESET}"
    mkdir -p ${loc[remote]}
    cp -f -u -r -p ${loc[local]} ${loc[remote]}
    if [ $? -eq 0 ]; then
        printf "${GREEN} OK"
    else
        printf "${RED} ERROR"
    fi
    printf "\n"
done
